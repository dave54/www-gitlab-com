---
layout: job_family_page
title: Senior Manager, Self-Service Customer Engagement
---

This Senior Manager, Self-Service Customer Engagement role is part of a team focused on building an easy self-service experience for our customers.

## Role
The Senior Manager, Self-Service Customer Engagement reports to the Director, Self Service Growth. 

### Job Grade
The Senior Manager, Self-Service Customer Engagement is a [grade 9](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities
* Design and implement new ways of engaging with our customers.  Including role definitions, role playbook and any required technology solutions.
* Build an innovative customer facing team that is convenient for our customers and acts as a self-service advocate.  
* Influence enhancement and self-service feature backlog based on customer feedback and instrumentation of your customer facing team.
* Build a team of top talent motivated around a common goal of best helping our customers self-service.
* Contribute to customer personas, customer journeys and the ideal self-service experience by market segment based on customer engagement.
* Anticipate problems and propose solutions related to sales operational changes (ex. quota, comp, territory carving, leads).
* Proactively identify and respond to customer issues within the self-service experience.  Act as the direct sales resource for our self-service product.
* Partner with marketing to deliver web experiences and content to optimize for self-service. Define campaigns, personas and customer cohorts for targeting.

### Requirements
* Ability to use GitLab
* Experience leading a front line customer facing team and owning key business performance outcomes.
* Proven track record of leadership driving innovation and change.
* Ability to manage sales / support roles as well as operational and strategic execution roles.
* Great project management skills. Ability to pull together disparate requirements, get agreement on priorities, communicate clearly and drive timelines.
* Experience with building and leading a front line sales or support team.
* Ability to develop and foster relations across the organization. (Including sales, product, marketing, customer success)
* Strong analytical skills and structured thinking. You know how to use data from disparate sources into a vision for what we should do.
* Resourcefulness to get things done cross-functionally.
* Operational execution and troubleshooting by identifying issues as they surface; proposing and executing changes to fix root causes.

### Performance Indicators
* Customer facing SLAs to be defined (e.g., time of first response, survey, CSAT).
* Increased adoption of self-service solutions (buying & account management) driving increased customer satisfaction, efficiency and Rep productivity.
* nARR performance of customers supported by the low touch self-service team.
* [Success Metrics exampled in Handbook](https://about.gitlab.com/handbook/sales/self-service/#success-metrics)

### Career Ladder
The next step for the Senior Manager, Self-Service Customer Engagement Family is to move to the Director, Self-Service Customer Engagement Job Family which has not yet been deined.

### Hiring Process
Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our team page.
* Selected candidates will be invited to schedule a 30min screening call with one of our Global Recruiters
* Next, candidates will be invited to schedule a first interview with the Hiring Manager
* Next, candidates will be invited to interview with 2-5 Team Members
* Final Interview will be with the Director of Online Self Service 

Additional details about our process can be found on our [hiring page](/handbook/hiring/).



